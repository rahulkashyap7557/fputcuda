#include <iostream>
#include <algorithm>
#include <cstdlib>
#include <string>
#include <cmath>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>
#include <cassert>
#include <iterator>
#include <iomanip>
#include <sstream>
#include <cuda.h>
#include <cuda_runtime.h>

#define pi 3.1415926535897932384626433
//#define nBlocks 16
//#define nThreads 32

//__global__ void fpuAcceleration(double * position, double * velocity, double * acceleration, double * alpha, double * beta, int * N){
//
//    int tid1 = threadIdx.x + blockIdx.x * blockDim.x;
//    double dxi, twodxi, dxipl1, dximn1, fac, fac1, fac2, fac13, fac23;
//	int j, k;
//
//
//
//    while (tid1 < *N)
//    {
//         j = tid1 - 1;
//         k = tid1 + 1;
//         if (j == -1) {
//			dximn1 = 0.0;
//		} else {
//			dximn1 = position[j];
//		}
//		if (k == *N) {
//			dxipl1 = 0.0;
//		} else {
//			dxipl1 = position[k];
//		}
//		dxi = position[tid1];
//		twodxi = 2.0 * dxi;
//		fac = dxipl1 + dximn1 - twodxi;
//		fac1 = dxipl1 - dxi;
//		fac2 = dxi - dximn1;
//		fac13 = fac1 * fac1 * fac1;
//		fac23 = fac2 * fac2 * fac2;
//		acceleration[tid1] = 2.*(*alpha) * fac + 4.* (*beta) * (fac13 - fac23);
//		tid1 += blockDim.x * gridDim.x;
//		//printf("%d\n", tid1);
//
//    }
//    __syncthreads();
//
//}

__global__ void velocityVerlet(double alpha, double beta, double * position, double * velocity, double * acceleration,
                               double * mass, int samplingFrequency, int numParticles, double dt){



    double dxi, twodxi, dxipl1, dximn1, fac, fac1, fac2, fac13, fac23;
	int i, j, k, tid;
	i = 0;
	double posTemp, velTemp;
	//printf("%lf \n", position[50]);



   // while (i < samplingFrequency){
           // tid = 0;
            tid = threadIdx.x + blockIdx.x * blockDim.x;
            while (tid < numParticles){
            //fpuAcceleration<<<10, 10>>>(position, velocity, acceleration, alpha, beta, *numParticles);
//            posTemp = position[tid];
//            velTemp = velocity[tid];

            position[tid] += (dt) *(velocity[tid]) + 0.5 * (dt) * (dt) *(acceleration[tid]);
            __syncthreads();
            //position[tid] = posTemp;
            __syncthreads();
            velocity[tid] +=  0.5 * (dt) * acceleration[tid];
            __syncthreads();
            //velocity[tid] = velTemp;
            //__syncthreads();
//            }
//
//
//        while (tid < numParticles){
               // Force law goes here. Could also be modified to launch another kernel to calculate acceleration in parallel.
               j = tid - 1;
               k = tid + 1;
          if (j == -1) {
			dximn1 = 0.0;
		} else {
			dximn1 = position[j];
		}
		if (k == numParticles) {
			dxipl1 = 0.0;
		} else {
			dxipl1 = position[k];
		}
		dxi = position[tid];
		twodxi = 2.0 * dxi;
		fac = dxipl1 + dximn1 - twodxi;
		fac1 = dxipl1 - dxi;
		fac2 = dxi - dximn1;
		fac13 = fac1 * fac1 * fac1;
		fac23 = fac2 * fac2 * fac2;
		acceleration[tid] = 2.*(alpha) * fac + 4.* (beta) * (fac13 - fac23);
		//tid1 += blockDim.x * gridDim.x;
       // }
		__syncthreads();

		//while (tid < numParticles){
               velocity[tid] += 0.5 * (dt) * acceleration[tid];
		//}
		__syncthreads();
        //velocity[tid] = velTemp;
		//printf("%f\n", acceleration[50]);
		//__syncthreads();
		//printf("%d\n", tid);

        //}
        //__syncthreads();
        tid += blockDim.x * gridDim.x;
        }
        //printf("%lf \n", position[50]);

       //i++;
   // }

}

__global__ void printFn()
{
    printf("Hello world from GPU!\n");
}



//__global__ void velocityVerletFull(double * alpha, double * beta, double * position, double * velocity, double * acceleration,
//                               double * mass, int * samplingFrequency, int * numParticles, double * dt){
//                                   int tid;
//
//        tid = threadIdx.x + blockIdx.x * blockDim.x;
//
//		velocity[tid] += 0.5 * (*dt) * acceleration[tid];
//		__syncthreads();
//		tid += blockDim.x * gridDim.x;
//		__syncthreads();
//
//
//}



int main(){

   // printFn<<<10, 10>>>();

   // Initialize variables on CPU
   const int numParticles = 100;
   const int nBlocks = 10;
   const int nThreads = 10;

   double position[numParticles];
   double velocity[numParticles];
   double acceleration[numParticles];
   double mass[numParticles];
   double ke[numParticles];
   double alpha = 0.00;
   double beta = 1.00;
   double A = 0.100;
   double dt = 0.01;
   int samplingFrequency = 10;
   int totalSystemTime = 10000;
   FILE *positionFile;
   FILE *velocityFile;
   FILE *accelerationFile;
   FILE *keFile;
   FILE *totalEnergyFile;

   positionFile = fopen("position.dat", "w");
   velocityFile = fopen("velocity.dat", "w");
   accelerationFile = fopen("acceleration.dat", "w");
   keFile = fopen("ke.dat", "w");
   totalEnergyFile = fopen("totalEnergy.dat", "w");

   int i;

   for (i = 0; i < numParticles; i++){

      mass[i] = 1.00;
      velocity[i] = 0.;
      position[i] = 0.;
      acceleration[i] = 0.;

   }

   position[49] = A;
   position[50] = -A;



   // Initialize Variables on GPU. Prefix d_ stands for device variable

   // Initialize device pointers on GPU



   double *d_position;
   double *d_velocity;
   double *d_acceleration;
   double *d_mass;

   double *d_alpha;
   double *d_beta;
   int *d_samplingFrequency;
   int *d_numParticles;
   double *d_dt;

   // Allocate memory on GPU

   cudaMalloc(&d_alpha, sizeof(double));
   cudaMalloc(&d_beta, sizeof(double));
   cudaMalloc(&d_dt, sizeof(double));
   cudaMalloc(&d_samplingFrequency, sizeof(int));

   cudaMalloc(&d_position, numParticles*sizeof(double));
   cudaMalloc(&d_velocity, numParticles*sizeof(double));
   cudaMalloc(&d_acceleration, numParticles*sizeof(double));
   cudaMalloc(&d_mass, numParticles*sizeof(double));
   cudaMalloc(&d_numParticles, sizeof(int));

    //Copy initialized variables to GPU

   cudaMemcpy(d_alpha, &alpha, sizeof(double), cudaMemcpyHostToDevice);
   cudaMemcpy(d_beta, &beta, sizeof(double), cudaMemcpyHostToDevice);
   cudaMemcpy(d_samplingFrequency, &samplingFrequency, sizeof(int), cudaMemcpyHostToDevice);
   cudaMemcpy(d_dt, &dt, sizeof(double), cudaMemcpyHostToDevice);
   cudaMemcpy(d_numParticles, &numParticles, sizeof(int), cudaMemcpyHostToDevice);
   cudaMemcpy(d_position, &position, numParticles*sizeof(double), cudaMemcpyHostToDevice);
   cudaMemcpy(d_velocity, &velocity, numParticles*sizeof(double), cudaMemcpyHostToDevice);
   cudaMemcpy(d_acceleration, &acceleration, numParticles*sizeof(double), cudaMemcpyHostToDevice);
   cudaMemcpy(d_mass, &mass, numParticles*sizeof(double), cudaMemcpyHostToDevice);

   // Kernel goes here

   //int i;
   int j, k;
   double  tke, tpe, te, cmass, dx, fac, y;

   i = 0;
   int w = 1;
  //

   while (i < totalSystemTime){
//     while (i < 10){
//        for ( int w = 0; w < samplingFrequency; w++){
        cudaMemcpy(d_alpha, &alpha, sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_beta, &beta, sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_samplingFrequency, &samplingFrequency, sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_numParticles, &numParticles, sizeof(int), cudaMemcpyHostToDevice);
        cudaMemcpy(d_position, &position, numParticles*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_velocity, &velocity, numParticles*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_acceleration, &acceleration, numParticles*sizeof(double), cudaMemcpyHostToDevice);
        cudaMemcpy(d_mass, &mass, numParticles*sizeof(double), cudaMemcpyHostToDevice);




       velocityVerlet<<<nBlocks, nThreads>>>(alpha, beta, d_position, d_velocity, d_acceleration, d_mass, samplingFrequency, numParticles, dt);
       cudaDeviceSynchronize();
      // fpuAcceleration<<<nBlocks, nThreads>>>(d_position, d_velocity, d_acceleration, d_alpha, d_beta, d_numParticles);
      // velocityVerletFull<<<nBlocks, nThreads>>>(d_alpha, d_beta, d_position, d_velocity, d_acceleration, d_mass, d_samplingFrequency, d_numParticles, d_dt);

        cudaMemcpy(&position, d_position, numParticles*sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(&velocity, d_velocity, numParticles*sizeof(double), cudaMemcpyDeviceToHost);
        cudaMemcpy(&acceleration, d_acceleration, numParticles*sizeof(double), cudaMemcpyDeviceToHost);
       //printf("%d\n", w);

       // Copy memory back to CPU




       // Compute energies and write to file

      //     if (w == samplingFrequency){



               printf("complete: %.3f%%\n",i*100./totalSystemTime);
                    tke = tpe = te = dx = 0.0;
                    cmass = 0.0;
                    for (j = 0; j < numParticles; j++) {
                        ke[j] = 0.5 * velocity[j] * velocity[j];
                        fprintf(keFile,"%.10f\t",ke[j]);
                        tke += ke[j];
                        k = j-1;
                        if (k == -1) {
                            dx = position[j];
                        } else {
                            dx = position[j] - position[k];
                        }
                        fac = dx * dx;
                        tpe += alpha * fac + beta * fac * fac;
                        cmass += position[j];
                    }
        			//fprintf(fp5, "%d\t%.10f\n", i, cmass);
                    cmass /= numParticles;
                    fprintf(keFile,"\n");
                    tpe += alpha * position[numParticles-1];
                    te = tpe + tke;
                    fprintf(totalEnergyFile,"%d\t%.10f\n", i, te);

                    for (k=0; k < numParticles; k++) {
                        y = position[k] - cmass;
                        fprintf(positionFile,"%.10f\t", y);
                    }
                    fprintf(positionFile,"\n");

                    for (k=0; k < numParticles; k++) {
                        fprintf(velocityFile,"%.10f\t", velocity[k]);
                    }
                    fprintf(velocityFile,"\n");

                    for (k=0; k < numParticles; k++) {
                        fprintf(accelerationFile,"%.10f\t", acceleration[k]);
                    }
                    fprintf(accelerationFile,"\n");
                    i++;
                    w = 1;
             //       }
                    w++;

               // printf("%d\n", i);







   }









   //Clean up device memory

   cudaFree(d_alpha);
   cudaFree(d_beta);
   cudaFree(d_dt);
   cudaFree(d_samplingFrequency);

   cudaFree(d_position);
   cudaFree(d_velocity);
   cudaFree(d_acceleration);
   cudaFree(d_mass);
   cudaFree(d_numParticles);



   return 0;




}


